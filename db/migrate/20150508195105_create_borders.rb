class CreateBorders < ActiveRecord::Migration
  def change
    create_table :borders do |t|
      t.integer :country1
      t.integer :country2

      t.timestamps null: false
    end
  end
end
