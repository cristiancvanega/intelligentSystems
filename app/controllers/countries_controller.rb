class CountriesController < ApplicationController
  before_action :set_country, only: [:show, :edit, :update, :destroy]

  # GET /countries
  # GET /countries.json
  def index
    @countries = Country.all
  end

  # GET /countries/1
  # GET /countries/1.json
  def show
  end

  # GET /countries/new
  def new
    @country = Country.new
  end

  # GET /countries/1/edit
  def edit
  end

  # GET /countries/1/listBorderCountry
  def listBorderCountry
    @country = Country.find(params[:country_id])
    @borders = Border.where("country1 = #{params[:country_id]} or country2 = #{params[:country_id]}")
    @responseBorders = Array.new
    j = 0
    @borders.each do |border|
      if border.country1 == params[:country_id].to_i
        @responseBorders[j] = Country.find(border.country2).name
        j = j + 1
      end
      if border.country2 == params[:country_id].to_i
        @responseBorders[j] = Country.find(border.country1).name
        j = j + 1
      end
    end

  end

  # GET /countries/1/translimite
  def translimite
    @country1 = Country.find(params[:country1]).name
    @country2 = Country.find(params[:country2]).name
    @borders11 = Border.where("country1 = #{params[:country1]}")
    @borders12 = Border.where("country2 = #{params[:country1]}")
    borders21 = Border.where("country1 = #{params[:country2]}")
    borders22 = Border.where("country2 = #{params[:country2]}")
    @borderResponse = Array.new
    @b1 = Array.new
    b2 = Array.new
    j = 0
    k = 0

    @borders11.each do |border|
      @b1[j] = border.country2
      j = j + 1
    end

    @borders12.each do |border|
      @b1[j] = border.country1
      j = j + 1
    end

    j=0
    borders21.each do |border|
      b2[j] = border.country2
      j = j + 1
    end

    borders22.each do |border|
      b2[j] = border.country1
      j = j + 1
    end

    for i in 0..@b1.size
      for l in 0..b2.size
        if @b1[i] == b2[l]
          @borderResponse[k] = @b1[i]
          k = k + 1
        end
      end
    end
    @borderResponse = Country.find(@borderResponse)
  end

  # POST /countries
  # POST /countries.json
  def create
    @country = Country.new(country_params)

    respond_to do |format|
      if @country.save
        format.html { redirect_to @country, notice: 'Country was successfully created.' }
        format.json { render :show, status: :created, location: @country }
      else
        format.html { render :new }
        format.json { render json: @country.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /countries/1
  # PATCH/PUT /countries/1.json
  def update
    respond_to do |format|
      if @country.update(country_params)
        format.html { redirect_to @country, notice: 'Country was successfully updated.' }
        format.json { render :show, status: :ok, location: @country }
      else
        format.html { render :edit }
        format.json { render json: @country.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /countries/1
  # DELETE /countries/1.json
  def destroy
    @country.destroy
    respond_to do |format|
      format.html { redirect_to countries_url, notice: 'Country was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_country
      @country = Country.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def country_params
      params.require(:country).permit(:name, :extension)
    end
end
