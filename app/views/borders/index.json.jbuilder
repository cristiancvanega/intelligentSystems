json.array!(@borders) do |border|
  json.extract! border, :id, :country1, :country2
  json.url border_url(border, format: :json)
end
